package tun2ray

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"
	"strings"
	"sync"
	"syscall"
	"time"

	"tun2ray/v2ray"

	"github.com/eycorsican/go-tun2socks/core"
	vcore "v2ray.com/core"
	vproxyman "v2ray.com/core/app/proxyman"
	vbytespool "v2ray.com/core/common/bytespool"
	verrors "v2ray.com/core/common/errors"
	v2stats "v2ray.com/core/features/stats"
	"v2ray.com/core/infra/conf"
	vinternet "v2ray.com/core/transport/internet"
)

type IOutboundServer interface {
	NewOutboundServer(Address string,
		Port int,
		UserID string,
		AlterID int,
		Level int,
		Security string) *OutboundServer
}

type OutboundServer struct {
	Address  string
	Port     int
	UserID   string
	AlterID  int
	Level    int
	Security string // auto, aes-128-cfb...
}

func (this *OutboundServer) NewOutboundServer(Address string,
	Port int,
	UserID string,
	AlterID int,
	Level int,
	Security string) *OutboundServer {
	this.Address = Address
	this.Port = Port
	this.UserID = UserID
	this.AlterID = AlterID
	this.Level = Level
	this.Security = Security

	return this
}

type ITLSSetting interface {
	NewTLSSetting(ServerName string,
		AllowInsecure bool,
		AllowInsecureCiphers bool) *TLSSetting
}

type TLSSetting struct {
	ServerName           string
	AllowInsecure        bool
	AllowInsecureCiphers bool
}

func (this *TLSSetting) NewTLSSetting(ServerName string,
	AllowInsecure bool,
	AllowInsecureCiphers bool) *TLSSetting {
	this.ServerName = ServerName
	this.AllowInsecure = AllowInsecure
	this.AllowInsecureCiphers = AllowInsecureCiphers
	return this
}

type ITransportSetting interface {
	// keep a method, make java class implement interface...
	NetworkName() string
}

type TCPSetting struct {
	//header
	HeaderType string //none, http
	// Headers    map[string]string
	Headers string //json array
}

func (s *TCPSetting) NetworkName() string {
	return "tcp"
}

func GetTCPSetting(i ITransportSetting) *TCPSetting {
	s, ok := i.(*TCPSetting)
	if ok {
		return s
	}
	return nil
}

type WebSocketSetting struct {
	Path string
	// Headers map[string]string // can't export to java
	Headers string //json array
}

func (s *WebSocketSetting) NetworkName() string {
	return "ws"
}

func GetWebSocketSetting(i ITransportSetting) *WebSocketSetting {
	s, ok := i.(*WebSocketSetting)
	if ok {
		return s
	}
	return nil
}

// gobind not supprt nested struct...
type VmessConfig struct {
	// DSN servers list split by comma: 1.1.1.1:53,8.8.8.8:53
	// Internal DNS used for ip routing
	DNSInternal string
	// DNS used in outbound
	/*
			"outbounds": [
		    {
		        "tag": "dns-out",
		        "protocol": "dns",
		        "settings": {
		            "network": "tcp",
		            "address": "1.1.1.1",
		            "port": 53
		        }
		    }
		]
	*/
	DNSOutbound string //1.1.1.1

	Loglevel string

	// only sinlge vnext
	IOutboundServer IOutboundServer

	// OutboundStreamSetting OutboundStreamSetting
	Network           string // TCP, mKCP, WS, HTTP/2, QUIC
	Security          string // tls, none
	ITransportSetting ITransportSetting
	// tls settings
	ITLSSetting ITLSSetting

	// mux
	MuxEnabled     bool
	MuxConcurrency int

	// Routing
	RouteMode int
	// https://www.v2ray.com/en/configuration/routing.html
	DomainStrategy string // : "AsIs" | "IPIfNonMatch" | "IPOnDemand"
	JSONRuleList   string //[]Rules
}

func (vconfig *VmessConfig) GetOutboundServer() *OutboundServer {
	return vconfig.IOutboundServer.(*OutboundServer)
}

func (vconfig *VmessConfig) GetTLSSetting() *TLSSetting {
	return vconfig.ITLSSetting.(*TLSSetting)
}

// VpnService should be implemented in Java/Kotlin.
type VpnService interface {
	// Protect is just a proxy to the VpnService.protect() method.
	// See also: https://developer.android.com/reference/android/net/VpnService.html#protect(int)
	Protect(fd int) bool
}

// PacketFlow TUN <-> tun2socks
type PacketFlow interface {
	// WritePacket should writes to the TUN fd.
	// register this function at RegisterOutputFn
	// data flow: TUN <- tun2socks
	WritePacket(packet []byte)
	// ReadPacket Write IP packets to the lwIP stack.
	// Call NativeReadPacket in the main loop of
	// the VpnService in Java/Kotlin, which should reads packets from the TUN fd.
	// data flow: TUN -> tun2socks
	ReadPacket(packet []byte, n int)
	// {
	// if lwipStack != nil {
	// lwipStack.Write(packet)
	// }
	// }
}

func NativeReadPacket(packet []byte, n int) {
	if lwipStack != nil {
		lwipStack.Write(packet[:n])
	}
}

// var localDNS = "223.5.5.5:53"
// var err error
var lwipStack core.LWIPStack
var v *vcore.Instance
var statsManager v2stats.Manager
var isStopped = false
var runningMutex = &sync.Mutex{}

const (
	v2Asset = "v2ray.location.asset"
)

type errPathObjHolder struct{}

func newError(values ...interface{}) *verrors.Error {
	return verrors.New(values...).WithPathObj(errPathObjHolder{})
}

// see loadVmessConfig
func vmessConfig2CoreConfig(profile *VmessConfig) (*vcore.Config, error) {
	jsonConfig := &conf.Config{}
	jsonConfig.LogConfig = &conf.LogConfig{
		// AccessLog: "",
		// ErrorLog:  "",
		LogLevel: profile.Loglevel,
	}
	// https://github.com/Loyalsoldier/v2ray-rules-dat
	// TODO Internal DNS and Outbound DNS
	jsonConfig.DNSConfig = createDNSConfig(profile.RouteMode, profile.DNSInternal)

	// TODO update rules
	routing := v2ray.Routing{
		RouteMode:      profile.RouteMode,
		DomainStrategy: profile.DomainStrategy,
		JSONRuleList:   profile.JSONRuleList,
	}
	jsonConfig.RouterConfig = createRouterConfig(&routing)
	// TODO policy

	// }
	vmessOutboundDetourConfig := createVmessOutboundDetourConfig(profile)
	freedomOutboundDetourConfig := createFreedomOutboundDetourConfig()
	// order matters
	// GFWList mode, use 'direct' as default
	if profile.RouteMode == 4 {
		jsonConfig.OutboundConfigs = []conf.OutboundDetourConfig{
			freedomOutboundDetourConfig,
			vmessOutboundDetourConfig,
		}
	} else {
		jsonConfig.OutboundConfigs = []conf.OutboundDetourConfig{
			vmessOutboundDetourConfig,
			freedomOutboundDetourConfig,
		}
	}
	// stats
	jsonConfig.Stats = &conf.StatsConfig{}

	coreConfig, err := jsonConfig.Build()
	if err != nil {
		return nil, err
	}
	return coreConfig, nil
}

func StartV2Ray(
	packetFlow PacketFlow,
	vpnService VpnService,
	logService LogService,
	profile *VmessConfig,
	assetPath string) error {
	// configJSON, err := json.Marshal(profile)
	// if err != nil {
	// log.Println("config error: %v", err)
	// return err
	// }
	// return StartV2RayRaw(packetFlow, vpnService, logService, configJSON, assetPath)
	config, err := vmessConfig2CoreConfig(profile)
	if err != nil {
		return err
	}
	return StartV2RayRaw(packetFlow, vpnService, logService, []byte(config.String()), "protobuf", assetPath)
}

func StartV2RayJSON(
	packetFlow PacketFlow,
	vpnService VpnService,
	logService LogService,
	configBytes []byte,
	assetPath string) error {
	return StartV2RayRaw(packetFlow, vpnService, logService, configBytes, "json", assetPath)
}

func StartV2RayRaw(
	packetFlow PacketFlow,
	vpnService VpnService,
	logService LogService,
	configBytes []byte,
	configFormat string, //protobuf or json
	assetPath string) error {
	if packetFlow != nil {
		// if dbService != nil {
		// 	vsession.DefaultDBService = dbService
		// }

		if lwipStack == nil {
			// Setup the lwIP stack.
			lwipStack = core.NewLWIPStack()
		}

		// Assets
		os.Setenv("v2ray.location.asset", assetPath)
		// log
		registerLogService(logService)

		// Protect file descriptors of net connections in the VPN process to prevent infinite loop.
		protectFd := func(s VpnService, fd int) error {
			if s.Protect(fd) {
				return nil
			} else {
				return errors.New(fmt.Sprintf("failed to protect fd %v", fd))
			}
		}
		netCtlr := func(network, address string, fd uintptr) error {
			return protectFd(vpnService, int(fd))
		}
		//protect outbound
		vinternet.RegisterDialerController(netCtlr)
		//protect inbound
		vinternet.RegisterListenerController(netCtlr)

		// Share the buffer pool.
		core.SetBufferPool(vbytespool.GetPool(core.BufSize))

		// Start the V2Ray instance.
		instance, err := vcore.StartInstance(configFormat, configBytes)
		if err != nil {
			log.Fatalf("start V instance failed: %v", err)
			return err
		}
		if instance == nil {
			return newError("core instance is nil")
		}
		v = instance
		statsManager = v.GetFeature(v2stats.ManagerType()).(v2stats.Manager)

		// Configure sniffing settings for traffic coming from tun2socks.
		sniffingConfig := &vproxyman.SniffingConfig{
			Enabled:             true,
			DestinationOverride: strings.Split("tls,http", ","),
		}
		ctx := vproxyman.ContextWithSniffingConfig(context.Background(), sniffingConfig)

		// Register tun2socks connection handlers.
		// vhandler := v2ray.NewHandler(ctx, v)
		// core.RegisterTCPConnectionHandler(vhandler)
		// core.RegisterUDPConnectionHandler(vhandler)
		// inbounds
		core.RegisterTCPConnHandler(v2ray.NewTCPHandler(ctx, v))
		core.RegisterUDPConnHandler(v2ray.NewUDPHandler(ctx, v, 2*time.Minute))

		// Write IP packets back to TUN.
		core.RegisterOutputFn(func(data []byte) (int, error) {
			if !isStopped {
				packetFlow.WritePacket(data)
			}
			return len(data), nil
		})

		runningMutex.Lock()
		isStopped = false
		runningMutex.Unlock()
		logService.WriteLog(fmt.Sprintf("V2Ray %s started!", GetV2RayVersion()))
		return nil
	}
	return errors.New("packetFlow is null")
}

// StopV2Ray stop v2ray
func StopV2Ray() {
	runningMutex.Lock()
	defer runningMutex.Unlock()
	if !isStopped {
		isStopped = true
		if lwipStack != nil {
			lwipStack.Close()
			lwipStack = nil
		}
		if statsManager != nil {
			statsManager.Close()
			statsManager = nil
		}
		v.Close()
		v = nil
	}
	// vsession.DefaultDBService = nil
}

func V() bool {
	return v != nil
}

// QueryStats traffic statistics
// QueryStats("up") or QueryStats("down")
// ~/go/src/v2ray.com/core/proxy/vmess/outbound/outbound.go
func QueryStats(direct string) int64 {
	if statsManager == nil {
		return 0
	}
	// name := "vmess>>>" + "ssrray" + ">>>traffic>>>" + direct
	name := "user>>>" + "xxf098@github.com" + ">>>traffic>>>" + direct + "link"
	counter := statsManager.GetCounter(name)
	if counter == nil {
		return 0
	}
	return counter.Set(0)
}

func GetV2RayVersion() string {
	return vcore.Version()
}

func IsCoreRunning() bool {
	runningMutex.Lock()
	defer runningMutex.Unlock()
	return !isStopped
}

// SetNonblock puts the fd in blocking or non-blocking mode.
func SetNonblock(fd int, nonblocking bool) bool {
	err := syscall.SetNonblock(fd, nonblocking)
	if err != nil {
		return false
	}
	return true
}
