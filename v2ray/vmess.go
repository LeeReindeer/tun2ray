package v2ray

import "encoding/json"

type Hosts map[string]string

type DNS struct {
	Hosts Hosts `json:"hosts,omitempty"`
	// DSN servers list split by comma: 1.1.1.1:53,8.8.8.8:53
	// Internal DNS used for ip routing
	Servers string `json:"servers"`
	// DNS used in outbound
	/*
			"outbounds": [
		    {
		        "tag": "dns-out",
		        "protocol": "dns",
		        "settings": {
		            "network": "tcp",
		            "address": "1.1.1.1",
		            "port": 53
		        }
		    }
		]
	*/
	DNSOutbound string `json:"-"` //1.1.1.1
}

type Rules struct {
	IP          []string `json:"ip,omitempty"`
	OutboundTag string   `json:"outboundTag"`
	Type        string   `json:"type"`
	Domain      []string `json:"domain,omitempty"`
}
type Routing struct {
	RouteMode      int               `json:"-"` //ignore in json
	DomainStrategy string            `json:"domainStrategy"`
	JSONRuleList   string            `json:"-"`
	RuleList       []Rules           `json:"rules"`
	RawRuleList    []json.RawMessage `json:"-"`
}

type InboundsSettings struct {
	Auth string `json:"auth"`
	IP   string `json:"ip"`
	UDP  bool   `json:"udp"`
}
type Inbounds struct {
	Listen           string            `json:"listen"`
	Port             int               `json:"port"`
	Protocol         string            `json:"protocol"`
	InboundsSettings *InboundsSettings `json:"settings,omitempty"`
	Tag              string            `json:"tag"`
}
type Log struct {
	Access   string `json:"access"`
	Error    string `json:"error"`
	Loglevel string `json:"loglevel"`
}
type Mux struct {
	Enabled     bool `json:"enabled"`
	Concurrency int  `json:"concurrency"`
}
type Users struct {
	AlterID  int    `json:"alterId"`
	Email    string `json:"email"`
	ID       string `json:"id"`
	Security string `json:"security"`
}
type Vnext struct {
	Address string  `json:"address"`
	Port    int     `json:"port"`
	Users   []Users `json:"users"`
}
type OutboundsSettings struct {
	Vnext          []Vnext `json:"vnext,omitempty"`
	DomainStrategy string  `json:"domainStrategy,omitempty"`
}
type Headers struct {
	Host string `json:"Host"`
}
type Wssettings struct {
	ConnectionReuse bool    `json:"connectionReuse"`
	Headers         Headers `json:"headers,omitempty"`
	Path            string  `json:"path"`
}

type QUICSettingsHeader struct {
	Type string `json:"type"`
}

type KCPSettingsHeader struct {
	Type string `json:"type"`
}
type TCPSettingsHeader struct {
	Type               string             `json:"type"`
	TCPSettingsRequest TCPSettingsRequest `json:"request"`
}

type TCPSettingsRequest struct {
	Version string      `json:"version"`
	Method  string      `json:"method"`
	Path    []string    `json:"path"`
	Headers HTTPHeaders `json:"headers"`
}

type HTTPHeaders struct {
	UserAgent      []string `json:"User-Agent"`
	AcceptEncoding []string `json:"Accept-Encoding"`
	Connection     string   `json:"Connection"`
	Pragma         string   `json:"Pragma"`
	Host           []string `json:"Host"`
}

type TLSSettings struct {
	AllowInsecure        bool `json:"allowInsecure"`
	AllowInsecureCiphers bool `json:"allowInsecureCiphers"`
}

type StreamSettings struct {
	Network     string       `json:"network"`
	Wssettings  Wssettings   `json:"wssettings"`
	Security    string       `json:"security,omitempty"`
	TLSSettings *TLSSettings `json:"tlsSettings,omitempty"`
}

type Outbounds struct {
	Mux            *Mux              `json:"mux,omitempty"`
	Protocol       string            `json:"protocol"`
	Settings       OutboundsSettings `json:"settings,omitempty"`
	StreamSettings *StreamSettings   `json:"streamSettings,omitempty"`
	Tag            string            `json:"tag"`
}
