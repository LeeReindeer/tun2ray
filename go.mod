module tun2ray

go 1.13

require (
	github.com/eycorsican/go-tun2socks v1.16.8
	github.com/xxf098/go-tun2socks-build v0.6.0
	golang.org/x/mobile v0.0.0-20191210151939-1a1fef82734d // indirect
	v2ray.com/core v4.19.1+incompatible
)
